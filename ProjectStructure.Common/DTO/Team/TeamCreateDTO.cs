﻿namespace ProjectStructure.Common.DTO.Team
{
    public class TeamCreateDTO
    {
        public string Name { get; set; }
    }
}